### References:

<table class="wrapped confluenceTable"><colgroup><col> <col> <col></colgroup> 

<tbody>

<tr>

<th class="numberingColumn confluenceTh">  
</th>

<th class="confluenceTh">Topic</th>

<th class="confluenceTh">Link</th>

</tr>

<tr>

<td class="numberingColumn confluenceTd">1</td>

<td class="confluenceTd"><span style="color: rgb(36,41,46);">Automated nginx proxy for Docker containers using docker-gen</span></td>

<td class="confluenceTd">[https://github.com/jwilder/nginx-proxy](https://github.com/jwilder/nginx-proxy)</td>

</tr>

<tr>

<td class="numberingColumn confluenceTd">2</td>

<td class="confluenceTd"><span style="color: rgb(36,41,46);">LetsEncrypt companion container for nginx-proxy</span></td>

<td class="confluenceTd">[https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion](https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion)</td>

</tr>

<tr>

<td class="numberingColumn confluenceTd" colspan="1">3</td>

<td colspan="1" class="confluenceTd"><span style="color: rgb(36,41,46);">Generate files from docker container meta-data</span></td>

<td colspan="1" class="confluenceTd">[https://github.com/jwilder/docker-gen](https://github.com/jwilder/docker-gen)</td>

</tr>

</tbody>

</table>

## BridgeAsia Official Web Implementation

### Server Information

<table class="wrapped confluenceTable"><colgroup><col> <col> <col></colgroup> 

<tbody>

<tr>

<th class="confluenceTh">Domain name</th>

<th class="confluenceTh">IP Address</th>

<th colspan="1" class="confluenceTh">User name</th>

</tr>

<tr>

<td class="confluenceTd">bridgeasiagroup.com</td>

<td class="confluenceTd">188.166.245.251</td>

<td colspan="1" class="confluenceTd">web</td>

</tr>

</tbody>

</table>

![](https://bridgeasia.atlassian.net/wiki/download/thumbnails/294920/image2017-8-13_20-11-14.png?version=1&modificationDate=1502629879886&cacheVersion=1&api=v2&width=600 "Software Development > Docker + Reverse proxy + LetsEncrypt > image2017-8-13_20-11-14.png")

URL:

[https://www.bridgeasiagroup.com/](https://www.bridgeasiagroup.com/)

[https://bridgeasiagroup.com/](https://bridgeasiagroup.com/)

### Files and directory paths

<table class="wrapped confluenceTable"><colgroup><col> <col> <col> <col></colgroup> 

<tbody>

<tr>

<th class="numberingColumn confluenceTh">  
</th>

<th class="confluenceTh">Purpose</th>

<th class="confluenceTh">Path</th>

<th class="confluenceTh">Remark</th>

</tr>

<tr>

<td class="numberingColumn confluenceTd">1</td>

<td class="confluenceTd">BridgeAsia web Nginx [configuration file](/wiki/download/attachments/294920/bridgeasiagroup.com.conf?version=1&modificationDate=1502638127936&cacheVersion=1&api=v2)</td>

<td class="confluenceTd">

<pre>/home/web/nginx/bridgeasia/conf.d/bridgeasiagroup.com.conf</pre>

</td>

<td class="confluenceTd">

*   Listen at port 80
*   gzip enabled
*   index.html

</td>

</tr>

<tr>

<td class="numberingColumn confluenceTd">2</td>

<td class="confluenceTd">[Nginx template file](/wiki/download/attachments/294920/nginx.tmpl?version=1&modificationDate=1502634412395&cacheVersion=1&api=v2) for <span style="color: rgb(36,41,46);">docker-gen container</span></td>

<td class="confluenceTd">

<pre>/home/web/nginx/nginx.tmpl</pre>

</td>

<td class="confluenceTd">

Download from

<pre>curl <span class="nolink">https://raw.githubusercontent.com/jwilder/nginx-proxy/master/nginx.tmpl  

and modify to not display nginx version  

server_tokens off;</span> </pre>

</td>

</tr>

<tr>

<td class="numberingColumn confluenceTd">3</td>

<td class="confluenceTd">Directory for LetsEncrypt certification files</td>

<td class="confluenceTd">

<pre>/opt/certs  

After certs created:  

/opt/certs/bridgeasiagroup.com/</pre>

</td>

<td class="confluenceTd">Create with the command below:

<pre>sudo mkdir -p /opt/certs</pre>

</td>

</tr>

<tr>

<td class="numberingColumn confluenceTd" colspan="1">4</td>

<td colspan="1" class="confluenceTd">BridgeAsia web content</td>

<td colspan="1" class="confluenceTd">

<pre>/home/web/bridgeasiagroup</pre>

</td>

<td colspan="1" class="confluenceTd">

<pre>Repository: git@bitbucket.org:kamolcu/bridgeasia_web.git</pre>

</td>

</tr>

</tbody>

</table>

### Set up instructions

<table class="wrapped relative-table confluenceTable" style="width: 84.8913%;"><colgroup><col style="width: 1.85778%;"> <col style="width: 23.3184%;"> <col style="width: 60.0256%;"> <col style="width: 14.7982%;"></colgroup> 

<tbody>

<tr>

<th class="numberingColumn confluenceTh">  
</th>

<th class="confluenceTh">Step</th>

<th class="confluenceTh">Command</th>

<th class="confluenceTh">Remark</th>

</tr>

<tr>

<td class="numberingColumn confluenceTd">1</td>

<td class="confluenceTd">Create docker network name: **docker_network**</td>

<td class="confluenceTd">

<pre>docker network create docker_network</pre>

</td>

<td class="confluenceTd">  
</td>

</tr>

<tr>

<td class="numberingColumn confluenceTd">2</td>

<td class="confluenceTd">

First start nginx (official image) with volumes

</td>

<td class="confluenceTd">

<pre>docker run -d -p 80:80 -p 443:443 \  
 --name nginx \  
 -v /etc/nginx/conf.d \  
 -v /etc/nginx/vhost.d \  
 -v /usr/share/nginx/html \  
 -v /opt/certs:/etc/nginx/certs:ro \  
 --net docker_network \  
 --restart=always \  
 --label com.github.jrcs.letsencrypt_nginx_proxy_companion.nginx_proxy \  
 nginx</pre>

</td>

<td class="confluenceTd">

*   docker_network
*   restart=always

</td>

</tr>

<tr>

<td class="numberingColumn confluenceTd">3</td>

<td class="confluenceTd">

Second start the docker-gen

container with the shared volumes and the template file

</td>

<td class="confluenceTd">

<pre>docker run -d \  
 --name nginx-gen \  
 --volumes-from nginx \  
 -v /home/web/nginx/nginx.tmpl:/etc/docker-gen/templates/nginx.tmpl:ro \  
 -v /var/run/docker.sock:/tmp/docker.sock:ro \  
 --net docker_network \  
 --label com.github.jrcs.letsencrypt_nginx_proxy_companion.docker_gen \  
 --restart=always \  
 jwilder/docker-gen \  
 -notify-sighup nginx -watch -wait 5s:30s /etc/docker-gen/templates/nginx.tmpl /etc/nginx/conf.d/default.conf</pre>

</td>

<td class="confluenceTd">

*   docker_network
*   restart=always

</td>

</tr>

<tr>

<td class="numberingColumn confluenceTd" colspan="1">4</td>

<td colspan="1" class="confluenceTd"><span style="color: rgb(36,41,46);">Then start this container</span></td>

<td colspan="1" class="confluenceTd">

<pre>docker run -d \  
 --name nginx-letsencrypt \  
 --volumes-from nginx \  
 -v /opt/certs:/etc/nginx/certs:rw \  
 -v /var/run/docker.sock:/var/run/docker.sock:ro \  
 --net docker_network \  
 --restart=always \  
 jrcs/letsencrypt-nginx-proxy-companion</pre>

</td>

<td colspan="1" class="confluenceTd">

*   docker_network
*   restart=always

</td>

</tr>

<tr>

<td class="numberingColumn confluenceTd" colspan="1">5</td>

<td colspan="1" class="confluenceTd">Start **ba-web** container</td>

<td colspan="1" class="confluenceTd">

<pre>docker run -d --name ba-web \  
--net docker_network \  
-e "VIRTUAL_HOST=bridgeasiagroup.com,www.bridgeasiagroup.com" \  
-e "LETSENCRYPT_HOST=bridgeasiagroup.com,www.bridgeasiagroup.com" \  
-e "LETSENCRYPT_EMAIL=adminfoo@bridgeasiagroup.com" \  
-e VIRTUAL_NETWORK=docker_network \  
--restart=always \  
-v /home/web/bridgeasia:/var/www/bridgeasia \  
-v /home/web/nginx/bridgeasia/conf.d:/etc/nginx/conf.d \  
nginx</pre>

</td>

<td colspan="1" class="confluenceTd">

*   docker_network
*   restart=always

</td>

</tr>

<tr>

<td class="numberingColumn confluenceTd" colspan="1">6</td>

<td colspan="1" class="confluenceTd">Add firewall rule to allow HTTPS access</td>

<td colspan="1" class="confluenceTd">

<div class="content-wrapper">

Networking → Firewalls → Create Firewall

![](https://bridgeasia.atlassian.net/wiki/download/thumbnails/294920/image2017-8-13_21-43-54.png?version=1&modificationDate=1502635442476&cacheVersion=1&api=v2&width=600 "Software Development > Docker + Reverse proxy + LetsEncrypt > image2017-8-13_21-43-54.png")

</div>

</td>

<td colspan="1" class="confluenceTd">Add port 80, 443</td>

</tr>

</tbody>

</table>