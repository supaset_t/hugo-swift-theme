+++
title = "Game"
description = "sss"
tags = [
    "go",
    "golang",
    "templates",
    "themes",
    "development",
]
date = "2014-04-02"
categories = [
    "Development",
    "golang",
]
image = "read.jpg"
+++


## Game Plan

*   [What, How and Who of Data Science](https://docs.google.com/presentation/d/1NuFoKqZmmoxvWg3tjpnh13BkMu8b_BIQbR7mpBKXjng/edit?usp=sharing)

*   Learning by doing

    1.  Image classification: fastai part1v3 [lesson 1](https://www.kaggle.com/cstorm3000/bridgeasia-bootcamp-fastai-part1v3-lesson-1) / [lesson 2](https://www.kaggle.com/cstorm3000/bridgeasia-bootcamp-fastai-part1v3-lesson-2)

    2.  Text classification: [thai2fit](https://github.com/cstorm125/thai2fit)

    3.  Tabular data regression

*   Become a SQL (and data manipulation) master

*   Deep-dive into how models work

[Google Cloud Platform Setup](https://course.fast.ai/start_gcp.html) (recommended because of $300 free credits for your projects)